import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/user/login',
      component: () => import('../views/user/LoginView.vue'),
    },
    {
      path: '/user/register',
      component: () => import('../views/user/RegisterView.vue'),
    },
    {
      path: '/user/center',
      component: () => import('../views/user/CenterView.vue'),
    }
  ]
})

export default router
