import axios, { type AxiosRequestConfig } from 'axios';

class Request {
  private service;
  constructor(config: AxiosRequestConfig) {
    this.service = axios.create(config);
  }

  public get(url: string, params?: object) {
    return this.service.get(url, { params });
  }

  public post(url: string, params?: object) {
    return this.service.post(url, params);
  }

  public put(url: string, params?: object) {
    return this.service.post(url, params);
  }

  public delete(url: string, params?: object) {
    return this.service.post(url, { params });
  }
}
const url = '/api';
const config: AxiosRequestConfig = {
  baseURL: url,
  timeout: 20 * 1000,
}

export default new Request(config);