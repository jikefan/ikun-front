import request from "@/utils/request";

export const register = async (account: string, password: string, checkPassword:string) => {
  return await request.post("/user/register", {
    account,
    password,
    checkPassword
  });
};

export const login = async (account: string, password: string) => {
  return await request.post("/user/login", {
    account,
    password,
  });
};

export const updatePassword = async (account:string, password: string) => 
  await request.post("/user/update", {
    account,
    password
  });