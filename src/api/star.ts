import request from '@/utils/request'

export const addStar = async (account: string | null, photoId: number) => 
    await request.post("/star/add", {
        account,
        photoId
    })