import request from '@/utils/request'

export const getAllPhoto = async () => {
  return await request.get('/photo/all');
}

export const getPhotoById = async (photoId: number) => {
  return await request.post('/photo/id', {
    photoId
  })
}