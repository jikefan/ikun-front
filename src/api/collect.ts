import request from '@/utils/request'

export const addCollect = async (account: string | null, photoId: number) => 
    await request.post("/collect/add", {
        account,
        photoId
    })

export const getCollectByAccount = async (account: string | null) => 
    await request.post("/collect/get", {
        account
    })

export const cancelCollectByCollectId = async (collectId: number | null) => 
    await request.post("/collect/cancel", {
        collectId
    })
